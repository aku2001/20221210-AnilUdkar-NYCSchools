//
//  SchoolCellViewModelTests.swift
//  20221210-AnilUdkar-NYCSchoolsTests
//
//  Created by Anil udkar on 12/13/22.
//  Copyright © 2022 AU. All rights reserved.
//

import XCTest
@testable import _0221210_AnilUdkar_NYCSchools

class SchoolCellViewModelTests: XCTestCase {

    var viewModel: SchoolViewModel!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        viewModel = SchoolViewModel(APIservice: NetworkServiceManagerMock())
        viewModel.fetchSchools(with: 0)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        viewModel = nil
    }

    func testCurrentPage() throws {
        XCTAssertEqual(viewModel.currentPage, 0)
    }
    
    func testNumberOfRows() throws {
        XCTAssertEqual(viewModel.numberOfRows, 5)
    }
    
    func testSchoolDetails() throws {
        let cellModel = viewModel.rowViewModel(for: IndexPath(row: 0, section: 0))
        XCTAssertEqual(cellModel?.schoolName, "Clinton School Writers & Artists, M.S. 260")
        XCTAssertEqual(cellModel?.phoneNumber, "212-524-4360")
    }
    
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Any test you write for XCTest can be annotated as throws and async.
        // Mark your test throws to produce an unexpected failure when your test encounters an uncaught error.
        // Mark your test async to allow awaiting for asynchronous code to complete. Check the results with assertions afterwards.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
