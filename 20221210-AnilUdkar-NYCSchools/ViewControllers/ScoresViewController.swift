//
//  DetailsViewController.swift
//  20221210-AnilUdkar-NYCSchools
//
//  Created by Anil udkar on 12/11/22.
//  Copyright © 2022 AU. All rights reserved.
//

import UIKit
import MapKit

class ScoresViewController: UIViewController {
    
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var schoolDesc: UILabel!
    @IBOutlet weak var testTakers: UILabel!
    @IBOutlet weak var readingScore: UILabel!
    @IBOutlet weak var mathScore: UILabel!
    @IBOutlet weak var writingScore: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    // view model object
    var viewModel: DetailsViewConfigurable! {
        didSet {
            setupUI()
        }
    }
    
    // lifecycle method
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    // lifecycle method
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    // Setup the UI
    private func setupUI() {
        if !isViewLoaded { return }
        //Given a time I would be improving the default/label value in better way in UI
        mathScore.text = viewModel.satScore?.satMathAvgScore ?? "NA"
        readingScore.text = viewModel.satScore?.satCriticalReadingAvgScore ?? "NA"
        writingScore.text = viewModel.satScore?.satWritingAvgScore ?? "NA"
        phoneNumber.text = ("Phone:  \(viewModel.schoolDetails.phoneNumber ?? "NA")")
        address.text = ("Address:  \(viewModel.schoolDetails.primaryAddressLine1 ?? "NA")")
        schoolDesc.text = viewModel.schoolDetails.overviewParagraph
        testTakers.text = viewModel.satScore?.numOfSatTestTakers ?? "NA"
        schoolName.text = viewModel.schoolDetails.schoolName
        
        if let location = viewModel.location {
            mapView.setRegion(location, animated: true)
        }
    }
    
}


