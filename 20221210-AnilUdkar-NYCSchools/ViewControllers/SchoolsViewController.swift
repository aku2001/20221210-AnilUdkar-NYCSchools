//
//  SchoolsViewController.swift
//  20221210-AnilUdkar-NYCSchools
//
//  Created by Anil udkar on 12/11/22.
//  Copyright © 2022 AU. All rights reserved.
//

import UIKit

class SchoolsViewController: UIViewController {
    
    var schoolViewModel: SchoolViewConfigurable!
    
    //**** If time permitted i could implement a search functionality
    //@IBOutlet weak var searchBar: UISearchBar!
    
    //table outlet
    @IBOutlet weak var tableView: UITableView!
    
    let cellRowHeight = 150
    
    var initialLoad = true
    private let activityView = UIActivityIndicatorView(style: .large)
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        schoolViewModel = SchoolViewModel()
        schoolViewModel.showAlert = { [weak self] msg in
            self?.showAlert(with: msg)
        }
        schoolViewModel.dataFetched = { [weak self] in
            self?.tableView.tableFooterView = nil
            self?.tableView.reloadData()
        }
        schoolViewModel.viewSchoolDetailsScreen = { [weak self] viewModel in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let schoolDetailsVC = storyboard.instantiateViewController(withIdentifier: "ScoresViewController") as? ScoresViewController else { return }
            schoolDetailsVC.viewModel = viewModel
            self?.present(schoolDetailsVC, animated: true, completion: nil)
        }
        
    }
    
    private func setupUI() {
        self.tableView.registerCell(type: SchoolTableViewCell.self)
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    /// Show alert with provided message
    private func showAlert(with msg: String) {
        let alert = UIAlertController(title: msg, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        present(alert, animated: true, completion: nil)
    }
    
    // lifecycle method
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if initialLoad {
            initialLoad = false
            schoolViewModel.fetchSchools(with: 0) //offset
        }
        
    }
    
    // spinner to show loading in UITableView footer during pagination
    private func createSpinnerFooter() -> UIView {
        let footerView = UIView(frame: CGRect(origin: .zero, size: CGSize(width: tableView.frame.size.width, height: 50.0)))
        let spinner = UIActivityIndicatorView()
        spinner.center = footerView.center
        footerView.addSubview(spinner)
        spinner.startAnimating()
        return footerView
    }
}

extension SchoolsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        schoolViewModel.rowClicked(for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(cellRowHeight)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if schoolViewModel.fetchNextCounts(index: indexPath.row) {
            tableView.tableFooterView = createSpinnerFooter()
        }
    }
}

extension SchoolsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolViewModel.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueCell(withType: SchoolTableViewCell.self, for: indexPath) as? SchoolTableViewCell, let schoolData = schoolViewModel.rowViewModel(for: indexPath) else {
            return UITableViewCell()
        }
        cell.configure(with: schoolData)
        return cell
    }
    
    
}
