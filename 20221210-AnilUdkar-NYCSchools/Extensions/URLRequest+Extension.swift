//
//  DetailTableViewCell.swift
//  20221210-AnilUdkar-NYCSchools
//
//  Created by Anil udkar on 12/12/22.
//  Copyright © 2022 AU. All rights reserved.
//

import UIKit

typealias Parameters = [String: String]

//Encoded with query param
extension URLRequest {
    func encode(with parameters: Parameters?) -> URLRequest {
        guard let parameters = parameters else { return self }
        var encodedURLRequest = self
        
        if let url = self.url,
           let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false),
           !parameters.isEmpty {
            var newUrlComponents = urlComponents
            let queryItems = parameters.map { key, value in
                URLQueryItem(name: key, value: value)
            }
            newUrlComponents.queryItems = queryItems
            encodedURLRequest.url = newUrlComponents.url
            return encodedURLRequest
        } else {
            return self
        }
    }
}
