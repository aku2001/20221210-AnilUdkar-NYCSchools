//
//  NetworkServiceManagerMOck.swift
//  20221210-AnilUdkar-NYCSchools
//
//  Created by Anil udkar on 12/13/22.
//  Copyright © 2022 AU. All rights reserved.
//

import Foundation

// Mock the network calls and respond
struct NetworkServiceManagerMock: NetworkServiceManagerProtocol {
    
    /// Read local json file
    private func readLocalFile(forName name: String) -> Data? {
            do {
                if let bundlePath = Bundle.main.path(forResource: name, ofType: "json"),
                   let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                    return jsonData
                }
            } catch {
                print(error)
            }
            return nil
        }
    
    func makeRequest<T>(withURL url: URL, method: HTTPMethod, params: [String : String], completion: @escaping (Result<[T], HTTPError>) -> Void) where T : Decodable {
        var data: Data?
        
        if url == schoolURL {
            data = readLocalFile(forName: "SchoolResponse")
        } else {
            data = readLocalFile(forName: "ScoreResponse")
        }
        
        if let data = data {
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let object = try decoder.decode([T].self, from: data)
                completion(.success(object))
            } catch {
                completion(.failure(.decodeError))
            }
        }
    }
}

    
