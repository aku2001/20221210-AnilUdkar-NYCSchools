//
//  Score.swift
//  20221210-AnilUdkar-NYCShools
//
//  Created by Anil udkar on 12/10/22.
//  Copyright © 2022 AKU. All rights reserved.
//
/*
 *** Sample score Json Object ***
 
 "dbn":"01M292",
 "school_name":"HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES",
 "num_of_sat_test_takers":"29",
 "sat_critical_reading_avg_score":"355",
 "sat_math_avg_score":"404",
 "sat_writing_avg_score":"363"
 
 */


import Foundation

struct Score: Codable {
    let dbn: String?
    let schoolName: String?
    let numOfSatTestTakers: String?
    let satCriticalReadingAvgScore: String?
    let satMathAvgScore: String?
    let satWritingAvgScore: String?
    
    enum CodingKeys: String, CodingKey {
        case schoolName
        case dbn,
             numOfSatTestTakers,
             satCriticalReadingAvgScore,
             satMathAvgScore,
             satWritingAvgScore
    }
}
