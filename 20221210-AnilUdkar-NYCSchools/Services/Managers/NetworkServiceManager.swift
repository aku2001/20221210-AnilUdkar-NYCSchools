//
//  ServiceManager.swift
//  20221210-AnilUdkar-NYCSchools
//
//  Created by Anil udkar on 12/11/22.
//  Copyright © 2022 AU. All rights reserved.
//

import Foundation

public struct HTTPMethod: RawRepresentable, Equatable, Hashable {
    
    public static let get = HTTPMethod(rawValue: "GET")
    public static let post = HTTPMethod(rawValue: "POST")
    
    public let rawValue: String
    
    public init(rawValue: String) {
        self.rawValue = rawValue
    }
}

//Error Enum for validation
enum HTTPError: Error {
    case urlFailed
    case noData
    case requestError
    case decodeError
}

//Interface
protocol NetworkServiceManagerProtocol {
    func makeRequest<T: Decodable>(withURL url: URL, method: HTTPMethod, params: [String: String], completion: @escaping (Result<[T], HTTPError>) -> Void)}


let schoolURL = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")!

let scoreURL = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")!

//Implement interface for network call
struct NetworkServiceManager: NetworkServiceManagerProtocol {
    
    public static let shared = NetworkServiceManager()
    
    func makeRequest<T: Decodable>(withURL url: URL, method: HTTPMethod, params: Parameters, completion: @escaping (Result<[T], HTTPError>) -> Void){
        
        let session = URLSession.shared
        var request = URLRequest(url: url).encode(with: params)
        request.httpMethod = method.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        session.dataTask(with: request as URLRequest) { (data, _, error) in
            DispatchQueue.main.async {
                guard let data = data, error == nil else {
                    completion(.failure(.noData))
                    return
                }
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                if let json = try? decoder.decode([T].self, from: data) {
                    completion(.success(json))
                } else {
                    completion(.failure(.decodeError))
                }
            }
        }.resume()
    }
}

