//
//  SchoolsViewConfigurable.swift
//  20221210-AnilUdkar-NYCSchools
//
//  Created by Anil udkar on 12/11/22.
//  Copyright © 2022 AU. All rights reserved.
//

// interface SchoolCellViewModel
protocol SchoolCellConfigurable {
    var schoolName: String? { get }
    var address: String? { get }
    var phoneNumber: String? { get }
    
}

// returns the value for cellmodel data
struct SchoolCellViewModel: SchoolCellConfigurable {
    
    // Holds the school  object
    let school: School
    
    // Computed properties
    var schoolName: String? { school.schoolName }
    var phoneNumber: String? { school.phoneNumber }
    var address: String? { school.primaryAddressLine1 }
    
    // Initilizer
    init(schoolDetails: School) {
        self.school = schoolDetails
    }
}
