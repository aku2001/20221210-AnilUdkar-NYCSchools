//
//  DetailsViewModel.swift
//  20221210-AnilUdkar-NYCSchools
//
//  Created by Anil udkar on 12/11/22.
//  Copyright © 2022 AU. All rights reserved.
//

import Foundation

import Foundation
import UIKit
import MapKit

///  interface detailsviewModel
protocol DetailsViewConfigurable {
    var schoolDetails: School {get set}
    var satScore: Score? {get set}
    var location: MKCoordinateRegion? { get }}

///  responsible for providing the details of school
struct ScoresViewModel: DetailsViewConfigurable {
    
    // Store school object
    var schoolDetails: School
    // Store score data object
    var satScore: Score?
    
    
    /// Computed property to get school location for Map
    var location: MKCoordinateRegion? {
        if let latitude = Double(schoolDetails.latitude ?? ""),
           let longitude = Double(schoolDetails.longitude ?? "") {
            let location = CLLocation(latitude: latitude, longitude: longitude)
            return MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 50, longitudinalMeters: 50)
        }
        return nil
    }
    
    // Initilizer
    init(_ schoolDetails: School, _ satScore: Score?) {
        self.schoolDetails = schoolDetails
        self.satScore = satScore
    }
}
