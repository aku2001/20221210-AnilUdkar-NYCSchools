//
//  SchoolsViewModel.swift
//  20221210-AnilUdkar-NYCSchools
//
//  Created by Anil udkar on 12/11/22.
//  Copyright © 2022 AU. All rights reserved.
//

import Foundation
import UIKit


// Defines the interface for SchoolsViewModel
protocol SchoolViewConfigurable {
    var numberOfRows: Int { get }
    var showAlert: ((String) -> Void)? { get set }
    var dataFetched: (() -> Void)? { get set }
    var viewSchoolDetailsScreen: ((DetailsViewConfigurable) -> Void)? { get set }
    
    func rowClicked(for indexPath: IndexPath)
    func fetchSchools(with offset: Int)
    func fetchNextCounts(index: Int) -> Bool
    func rowViewModel(for indexPath: IndexPath) -> SchoolCellViewModel?
}

// Responsible for api calls and display
class SchoolViewModel: SchoolViewConfigurable {
    
    // Network object
    let APIService: NetworkServiceManagerProtocol
    
    // Count per  request in each page
    let fetchCount = 100
    
    // Store the current page
    var currentPage = 0
    
    // Store the cell view models for school list
    var schoolCellViewModels: [SchoolCellViewModel] = []
    
    //  handler for alert
    var showAlert: ((String) -> Void)?
    
    // handler for data ready
    var dataFetched: (() -> Void)?
    
    // handler to show school details screen
    var viewSchoolDetailsScreen: ((DetailsViewConfigurable) -> Void)?
    
    // Initilizer
    init(APIservice: NetworkServiceManagerProtocol = NetworkServiceManager()) {
        self.APIService = APIservice
    }
    
    // Computed property returns count
    var numberOfRows: Int {
        return schoolCellViewModels.count
    }
    
    // Check and fetch next page of data
    func fetchNextCounts(index: Int) -> Bool {
        if (index == schoolCellViewModels.count - 1) {
            fetchSchools(with: currentPage + 1)
            return true
        }
        return false
    }
    
    // cell view model for indexPath
    func rowViewModel(for indexPath: IndexPath) -> SchoolCellViewModel? {
        if indexPath.row < schoolCellViewModels.count {
            return schoolCellViewModels[indexPath.row]
        } else {
            return nil
        }
    }
    
    // Fetch the SAT score for specific school and display
    func rowClicked(for indexPath: IndexPath) {
        if let viewModel = rowViewModel(for: indexPath) {
            fetchScore(for: viewModel.school) { [weak self] satScores in
                let schoolDetailsVM = ScoresViewModel(viewModel.school, satScores.first)
                self?.viewSchoolDetailsScreen?(schoolDetailsVM)
            }
        }
    }
    
    // Setup cell  models for schools data
    private func setupCellModels(for schools: [School]) {
        schoolCellViewModels.append(contentsOf: schools.map { SchoolCellViewModel.init(schoolDetails: $0) })
    }
}

// API Calls
extension SchoolViewModel {
    
    // Fetch the list of schools implemented with pagination
    func fetchSchools(with offset: Int) {
        let qParams = ["$limit": "\(fetchCount)","$offset": "\(offset)"]
        APIService.makeRequest(withURL: schoolURL,method: .get, params: qParams) { [weak self] (result: Result<[School], HTTPError>) in
            switch result {
            case .success(let schools):
                self?.currentPage = offset
                self?.setupCellModels(for: schools)
                self?.dataFetched?()
            case .failure(let error):
                self?.showAlert?(error.localizedDescription)
            }
        }
    }
    
    // Fetch the SAT score for specific school
    func fetchScore(for school: School, completion: @escaping ([Score]) -> Void) {
        guard let id = school.dbn else {
            completion([])
            return
        }
        let qParams = ["dbn": "\(id)"]
        APIService.makeRequest(withURL: scoreURL, method: .get, params: qParams) { (result: Result<[Score], HTTPError>) in
            switch result {
            case .success(let satScores):
                completion(satScores)
            case .failure:
                completion([])
            }
        }
    }
}
