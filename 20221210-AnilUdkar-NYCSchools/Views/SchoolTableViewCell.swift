//
//  SchoolTableViewCell.swift
//  20221210-AnilUdkar-NYCShools
//
//  Created by Anil udkar on 12/10/22.
//  Copyright © 2022 AKU. All rights reserved.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {
    
    // view model
    var viewModel: SchoolCellConfigurable!
    
    //outlets for nib file
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var schoolAddress: UILabel!
    
    // Cofigure cell
    func configure(with viewModel:SchoolCellConfigurable) {
        self.schoolName.text = viewModel.schoolName
        self.phoneNumber.text = viewModel.phoneNumber
        self.schoolAddress.text = viewModel.address
    }
    
    // Reset cell for reuse
    override func prepareForReuse() {
        schoolName.text = nil
        phoneNumber.text = nil
        schoolAddress.text = nil
        super.prepareForReuse()
    }
}
